<?php
include("autoload.php");

// 开启会话
session_start();


if (isset($_SESSION['user_id'])&& !empty($_SESSION['user_id'])) { // 用户是否已登录
	$conn = \libs\Conn::Conn();
	// var_dump($_SESSION['user_id']);exit;
	$query = "select order_id from order_list where user_id={$_SESSION['user_id']}";
	if ($result = $conn->query($query)) {
		$order = $result->fetch_all();
	}
	$result = [];
	array_map(function ($value) use (&$result) {
	    $result = array_merge($result,array_values($value));
	}, $order);
	$order = array_unique($result);
    include("view/personal.html");
   
}else{  //跳转到登录页面
	
    header("Location:login.php");
}

?>